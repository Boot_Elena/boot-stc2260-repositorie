// Домашняя работа №6 Object and String
// Ссылка на задачу
// https://gitlab.com/Boooch/stc-22-60/-/blob/develop/Homeworks/Homeworks.md

public class Main {
    public static void main(String[] args) {

        Human human = new Human( "Иван", "Петров", "Матвеевич", "Джава",
                "Марковцева", "24", "184", "25 45 265863");

        Human human2 = new Human( "Сергей", "Пупочкин", "Сергеевич", "Джава",
                "Марковцева", "29", "100", "25 45 265863");

        System.out.println(human.toString());
        System.out.println("  ");
        System.out.println(human2.toString());
        System.out.println("   ");

        System.out.println("Сравнение серии и номера паспорта");
        if (human.equals(human2)){
            System.out.println("Пу пу пу, а почему они одинаковые?");
        }
        else {
            System.out.println("Серия и номер разные");
        }
        System.out.println("  ");
        System.out.println("Вывод hashCode");
        System.out.println(" human - " + human.hashCode());
        System.out.println(" human2 - " + human2.hashCode());

    }
}

//    Создать класс Human у которого будут поля:
//
//private String name;
//private String lastName;
//private String patronymic;
//private String city;
//private String street;
//private String house;
//private String flat;
//private String numberPassport;
//
//        Переопределить три метода: toString(), hashCode(), equals
//        Метод toString должен выводить информацию таким образом (пример):
//        Пупкин Вася Варфаламеевич
//        Паспорт:
//        Серия: 98 22 Номер: 897643
//        Город Джава, ул. Программистов, дом 15, квартира 54
//        Метод equals() должен сравнивать людей по номеру паспорта