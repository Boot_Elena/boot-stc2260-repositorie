import java.util.Objects;

public class Human {

    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String numberPassport;


    public Human(String name, String lastName, String patronymic, String city, String street, String house, String flat, String numberPassport) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.numberPassport = numberPassport;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getFlat() {
        return flat;
    }

    public String getNumberPassport() {
        return numberPassport;
    }

    @Override
    public String toString() {
        String[] number = numberPassport.split(" ");
        return this.lastName + " " + name +" "+ patronymic + "\nПаспорт:" + "\nСерия: " + number[0] + " " + number[1] + " Номер: "+ number[2] + "\nГород " + city + " ул." + street +
                " Дом " + house + " Квартира " + flat ;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return numberPassport.equals(human.numberPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, patronymic, city, street, house, flat, numberPassport);
    }


}
