public class Main {
    public static void main(String[] args) {

// проверка на четность элемента

        int[] array = {26, 222, 1551, 34};
        int[] result;


        result = Sequence.filter(array, number -> number % 2 == 0);
        System.out.println(" Вывод четных значений массива ");
        for (int i = 0; i < result.length; i++) {
            System.out.print(" [ " + result[i] + " ] ");
        }
        System.out.println();

// проверка на четность суммы цифр элемента

        result = Sequence.filter(array, number -> {
            int sumChisel = 0;
            while (number != 0) {
                int prom = number % 10;
                sumChisel = sumChisel + prom;
                number = number / 10;

            }
            if (sumChisel % 2 == 0) {
                return true;
            } else {
                return false;
            }
        });

        System.out.println(" Вывод элементов чья сумма цифр элемента является четным числом ");
        for (int i = 0; i < result.length; i++) {
            System.out.print(" [ " + result[i] + " ] ");
        }
        System.out.println();

// проверка на четность каждой цифры элемента

        result = Sequence.filter(array, number -> {
            while (number != 0) {
                int a = number % 10;
                if (a % 2 == 0) {
                    number = number / 10;
                } else break;
            }
            if (number == 0) {
                return true;
            } else {
                return false;
            }
        });

        System.out.println(" Вывод элементов цифры которых четные ");
        for (int i = 0; i < result.length; i++) {
            System.out.print(" [ " + result[i] + " ] ");
        }
        System.out.println();


// проверка палиндромность элемента

        result = Sequence.filter(array, number -> {
            // Создаем реверс number
            int revNuber = 0;
            int copNumber = number;
            while (copNumber != 0) {
                revNuber = revNuber * 10 + (copNumber % 10);
                copNumber = copNumber / 10;
            }
//            Сверяем значения
            while (number != 0) {
                if (number % 10 == revNuber % 10) {
                    number = number / 10;
                    revNuber = revNuber / 10;
                }
                if (number % 10 != revNuber % 10) {
//                    number = number / 10;
                    break;
                }
            }
                if (number == 0) {
                    return true;
                } else {
                    return false;
                }

        });

        System.out.println(" Вывод элементов палиндромных ");
        for (int i = 0; i < result.length; i++) {
            System.out.print(" [ " + result[i] + " ] ");
        }
        System.out.println();
    }
}











//5. Лямбда выражения
// Предусмотреть функциональный интерфейс:
// interface ByCondition {
// boolean isOk(int number);
// Написать класс Sequence, в котором должен присутствовать метод filter:
//
// public static int[] filter(int[] array, ByCondition condition) {
// ...
// }
//
// Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
// В main в качестве condition подставить:
//
// проверку на четность элемента
// проверку, является ли сумма цифр элемента четным числом.
// Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
// Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).