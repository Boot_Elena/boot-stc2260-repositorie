public class Human {

    private int id;
    private String firstName;
    private String lastName;

    private int age;
    private boolean work;


    public Human(int id) {
        this.id = id;
    }

    public Human(String firstName, String lastName, int age, boolean work) {
        this.firstName = firstName;
        this.lastName = lastName;

        this.age = age;
        this.work = work;

    }

    public Human(int id, String firstName, String lastName,  int age, boolean work) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;

        this.age = age;
        this.work = work;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWork() {
        return work;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    @Override
    public String toString() {
        return id + "|" + firstName + "|" +  lastName + "|" + age + "|" + work;
    }
}
