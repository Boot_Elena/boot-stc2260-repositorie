import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        CrudHuman crudHuman = new UsersRepositoryFileImpl();

        File file = new File("Humans.txt");

        try {
            if (file.createNewFile()) {
                System.out.println("файл создан");
            } else {
                System.out.println("Файл Humans уже создан " + file.exists());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


            System.out.println("=== Введите цифру нужной задачи ===");
        Thread.sleep(1000);
        System.out.println("1 Поиск юзера по ID ");
        Thread.sleep(250);
        System.out.println("2 Создание нового юзера ");
        Thread.sleep(250);
        System.out.println("3 Обновление информации по юзеру ");
        Thread.sleep(250);
        System.out.println("4 Удаление юзера по ID ");
        Thread.sleep(250);
        System.out.print(" -");
        Thread.sleep(250);
        System.out.print(">");
        Thread.sleep(250);
        System.out.print("> ");



        Scanner scanner = new Scanner(System.in);
        int zadacha = scanner.nextInt();


        if (zadacha == 1){
            System.out.println("Выбрана задача поиск юзера по ID ");
            Thread.sleep(700);
            System.out.println("Введите нужный ID для поиска ");
            Thread.sleep(200);
            System.out.print(" -");
            Thread.sleep(200);
            System.out.print(">");
            Thread.sleep(200);
            System.out.print("> ");
            crudHuman.getById(scanner.nextInt());



        }
        else if (zadacha == 2){
            System.out.println("=======================================");
            System.out.println("2 Создание нового юзера");
            Thread.sleep(200);
            System.out.println("Введите имя");
            String firstName = scanner.next();

            System.out.println("Введите фамилию");
            String lastName = scanner.next();

            System.out.println("Введите возраст");
            int age = scanner.nextInt();

            System.out.println("Укажите наличие работы  да/нет");
            String workYN = scanner.next();

            String yes = "да";
            String no = "нет";

            boolean work;
            if (yes.equals(workYN)){
                work = true;
            }

            else if (no.equals(workYN)){
                work = false;
            }
            else {
                work = false;
            }

            crudHuman.create(firstName, lastName, age, work);

        }


        else if (zadacha == 3){
            System.out.println("3 Обновление информации юзера по id ");
            System.out.println("Введите ID");
            int id = scanner.nextInt();

            System.out.println("Введите новое имя");
            String firstName = scanner.next();

            System.out.println("Введите новую фамилию");
            String lastName = scanner.next();

            System.out.println("Введите новый возраст");
            int age = scanner.nextInt();

            System.out.println("Укажите наличие работы  да/нет");
            String workYN = scanner.next();

            String yes = "да";
            String no = "нет";

            boolean work;
            if (yes.equals(workYN)){
                work = true;
            }

            else if (no.equals(workYN)){
                work = false;
            }
            else {
                work = false;
            }
            crudHuman.update(id, firstName, lastName, age, work);

        }
        else if (zadacha == 4){
            System.out.println("4 Удаление юзера по ID ");
            Thread.sleep(700);
            System.out.println("Введите ID для удаления юзера ");
            int id = scanner.nextInt();
            crudHuman.delete(id);



        }

        else {
            System.out.println(" Выход ");

        }


    }
}