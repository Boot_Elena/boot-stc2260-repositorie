import java.io.*;

import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements CrudHuman {


    @Override
    public Human getById(int id) {


        System.out.println("поиск по id "+ id);
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Humans.txt"));
            String line = reader.readLine();
            String[] strings;
            int count = 0;

            if (line != null){
                while (line != null){
                    strings = line.split("\\|");

                    if (id == Integer.parseInt(strings[0])){
                        Human human = new Human(Integer.parseInt(strings[0]), strings[1], strings[2],
                                Integer.parseInt(strings[3]), Boolean.parseBoolean(strings[4]));
                        System.out.println("пользователь найден");
                        System.out.println(human);
                        return human;
                    }

                    line = reader.readLine();
                }
            }
            else if (line == null && id > count){
                System.out.println("пользователь не найден с ID " + id);
            }
            reader.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    @Override
    public Human create(String firstName, String lastName, int age, boolean work) {
        Human human = new Human(firstName, lastName, age, work);

        try
                (Writer writer = new FileWriter("Humans.txt", true)) {

            BufferedReader reader = new BufferedReader(new FileReader("Humans.txt"));
            String line = reader.readLine();
            String[] strings;
            int count = -1;

                while (line != null) {
                    strings = line.split("|");
                    count = Integer.parseInt(strings[0]);
                    line = reader.readLine();
                }

                if (count != -1){
                human.setId(count + 1);
            }

            writer.write(human.getId()+ "|" + firstName + "|" + lastName + "|" + age + "|" + work + "\n");
            System.out.println("новый пользователь создан");

            writer.flush();
            writer.close();
            reader.close();


        } catch (IOException e) {
            throw new RuntimeException(e);
        }

       return human;
    }

    @Override
    public Human update(int id, String firstName, String lastName, int age, boolean work) {
        Human human = new Human(id, firstName, lastName, age, work);


        try   {

            BufferedReader reader = new BufferedReader(new FileReader("Humans.txt"));
            List<Human> humans = new ArrayList<>();
            String line = reader.readLine();
            String[] strings;
            Human newHuman;
            int idOld;
            int ageOld;
            boolean workOld;

            String firstNameOld;
            String lastNameOld;


            while (line != null) {
                strings = line.split("\\|");

                if (id == Integer.parseInt(strings[0])){
                    newHuman = new Human(id, firstName, lastName, age, work);
                    humans.add(newHuman);

                }
                else if (id != Integer.parseInt(strings[0])) {
                    idOld = Integer.parseInt(strings[0]);
                    firstNameOld = strings[1];
                    lastNameOld = strings[2];
                    ageOld = Integer.parseInt(strings[3]);
                    workOld = Boolean.parseBoolean(strings[4]);

                    newHuman = new Human(idOld, firstNameOld, lastNameOld, ageOld, workOld);
                    humans.add(newHuman);
                }

                line = reader.readLine();
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter("Humans.txt"));

            for (int i = 0; i < humans.size(); i++) {
                writer.write(forAdd(humans.get(i)));
            }
            writer.flush();
            writer.close();
            reader.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return human;
    }




    @Override
    public void delete(int id) {

        try   {

            BufferedReader reader = new BufferedReader(new FileReader("Humans.txt"));
            List<Human> humansDel = new ArrayList<>();
            String line = reader.readLine();
            String[] strings;
            Human newHuman;
            int idOld;
            int idOldP = 0;
            int ageOld;
            boolean workOld;
            String lineDel = null;

            String firstNameOld;
            String lastNameOld;


            while (line != null) {
                strings = line.split("\\|");

                if (id == Integer.parseInt(strings[0])){
                    lineDel = line;
                }

                else if (id != Integer.parseInt(strings[0])) {
                    idOld = idOldP++;
                    firstNameOld = strings[1];
                    lastNameOld = strings[2];

                    ageOld = Integer.parseInt(strings[3]);
                    workOld = Boolean.parseBoolean(strings[4]);

                    newHuman = new Human(idOld, firstNameOld, lastNameOld, ageOld, workOld);
                    humansDel.add(newHuman);
                }

                line = reader.readLine();
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter("Humans.txt"));

            for (int i = 0; i < humansDel.size(); i++) {
                writer.write(forAdd(humansDel.get(i)));
            }

            System.out.println(" Юзер " + lineDel + "удален");

            writer.flush();
            writer.close();
            reader.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    private String forAdd(Human human) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(human.getId());
        stringBuilder.append("|");
        stringBuilder.append(human.getFirstName());
        stringBuilder.append("|");
        stringBuilder.append(human.getLastName());
        stringBuilder.append("|");
        stringBuilder.append(human.getAge());
        stringBuilder.append("|");
        stringBuilder.append(human.isWork());
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

}
