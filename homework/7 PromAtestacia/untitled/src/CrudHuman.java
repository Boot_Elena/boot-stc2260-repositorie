import java.io.IOException;

public interface CrudHuman {

    Human getById(int id);

    Human create(
            String firstName,
            String lastName,
            int age, boolean work);

    Human update(int id,
                 String firstName,
                 String lastName,
                 int age, boolean work);

    void delete(int id);
}


