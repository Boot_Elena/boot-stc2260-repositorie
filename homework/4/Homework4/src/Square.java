// потомок класса Rectangle
// реализует интерфейс Moveable
public class Square extends Rectangle implements Moveable{


    public Square(int x, int y, int storonaA) {
        super(x, y, storonaA);
    }

    @Override
    public void getPerimeter() {
        int perimeter = this.getStoronaA() * 4;
                System.out.println("Периметр квадрата равна" + perimeter );

    }

    public void znachenieKoordinat (){
        System.out.println("Координаты квадрата равны х = "+ this.getX() +" y = "+ this.getY());

    }

    public void move(int x, int y){
        System.out.println("квадрат перененсен по координате х на "+ x +" значений, по координате Y на "+ y +" значений");
        x = this.getX() + x;
        y = this.getY() + y;
        System.out.println("Новые координаты квадрата равны х = "+ x +"  Y = "+ y );

    }
}
