public class Main {

    public static void main(String[] args){

        // создание массива фигур
        Figure[] arrayFigures = new Figure[4];
        arrayFigures[0] = new Ellipse(3, 4, 5,8);
        arrayFigures[1] = new Rectangle(2, 6, 8, 7);
        arrayFigures[2] = new Square(7, 5, 1);
        arrayFigures[3] = new Circle(8, 3, 6);

        for(int i = 0; i < arrayFigures.length; i++){
            //вывод периметра фигур
            arrayFigures[i].getPerimeter();
            // вывод координат фигур
            arrayFigures[i].znachenieKoordinat();
        }

        // изменение координат фигур
        System.out.println("    ");
        ((Moveable) arrayFigures[3]).move((int) (Math.random() * 10),(int) (Math.random() * 10 + 1));
        System.out.println("    ");
        ((Moveable) arrayFigures[2]).move((int) (Math.random() * 10),(int) (Math.random() * 10 + 1));

    }

}
/*
        ((Moveable) arrayFigures[3]).move нашла пример этого способа в интернете, как это работает нет описания.
        не понятно, это только для интерфейса или для класса тоже работает?
        изначально пробовала через
        Circle cirkle = new Circle();
        cirkle.move(.....), но нужно было опять переменные вводить
        пробовала вместо "cirkle.move(.....)" -> "arrayFigures[3].move(.....)" была ошибка
        как все-таки это работает?

 */

/*4. OOП
        Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
        Классы Ellipse и Rectangle должны быть потомками класса Figure.
        Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
        В классе Figure предусмотреть абстрактный метод getPerimeter().
        Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит перемещать фигуру на заданные координаты.
        Данный интерфейс должны реализовать только классы Circle и Square.
        В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр, а у "перемещаемых" фигур изменить случайным образом координаты.
 */