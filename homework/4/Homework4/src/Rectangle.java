// потомок класса Figure
public class Rectangle extends Figure{

    private int storonaA;
    private int storonaB;

    public Rectangle(int x, int y, int storonaA, int storonaB) {
        super(x, y);
        this.storonaA = storonaA;
        this.storonaB = storonaB;
    }

    public Rectangle(int x, int y, int storonaA) {
        super(x, y);
        this.storonaA = storonaA;
    }

    @Override
    public void getPerimeter() {
        int perimeter = 2 * (storonaA + storonaB);
        System.out.println("Периметр прямоугольника равен " + perimeter);
    }

    public void znachenieKoordinat (){
        System.out.println("Координаты прямоугольника равны х = "+ this.getX() +" y = "+ this.getY());
    }

    public int getStoronaA() {
        return storonaA;
    }

    public int getStoronaB() {
        return storonaB;
    }

    public void setStoronaA(int storonaA) {
        this.storonaA = storonaA;
    }

    public void setStoronaB(int storonaB) {
        this.storonaB = storonaB;
    }
}
