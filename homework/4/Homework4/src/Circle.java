// потомок класса Ellips
// реализует интерфейс Moveable
public class Circle extends Ellipse implements Moveable{


    public Circle(int x, int y, int radius1) {
        super(x, y, radius1);
    }

    @Override
    public void getPerimeter() {

        double perimeter = 2 * Math.PI * getRadius1();
        System.out.println("Периметр круга равен = " + perimeter);
    }

    public void znachenieKoordinat (){
        System.out.println("Координаты круга равны х = "+ this.getX() +" y = "+ this.getY());
    }

    public void move(int x, int y){
        System.out.println("круг перененсен по координате х на "+ x +" значений, по координате Y на "+ y +" значений");
        x = this.getX() + x;
        y = this.getY() + y;
        System.out.println("Новые координаты круга равны х = "+ x +"  Y = "+ y );
    }


}
