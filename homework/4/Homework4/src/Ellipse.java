// потомок класса Figure

public class Ellipse extends Figure{

    private int radius1;
    private int radius2;

    public Ellipse(int x, int y, int radius1, int radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public Ellipse(int x, int y, int radius1) {
        super(x, y);
        this.radius1 = radius1;
    }


    @Override
    public void getPerimeter() {
        double perimeter = 2 * Math.PI * Math.sqrt(( Math.pow(radius1, 2) + Math.pow(radius2, 2)) / 2);
        System.out.println("Периметр эллипса равен " + perimeter);

    }

    public void znachenieKoordinat (){
        System.out.println("Координаты эллипса равны х = "+ this.getX() +" y = "+ this.getY());

    }

    public int getRadius1() {
        return radius1;
    }

    public int getRadius2() {
        return radius2;
    }

    public void setRadius1(int radius1) {
        this.radius1 = radius1;
    }

    public void setRadius2(int radius2) {
        this.radius2 = radius2;
    }
}
