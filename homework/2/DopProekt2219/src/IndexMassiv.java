// https://gitlab.com/Boooch/stc-22-60/-/blob/develop/Homeworks/Homeworks.md
// 3. Процедуры и функции
// 1 Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
import java.util.Scanner;

public class IndexMassiv {
    public static void main(String[] args){

        int[] numberMassiv = {10, 0, 15, 5, 35, 50};
        System.out.println("Введите искомое число");
        Scanner Scaner = new Scanner(System.in);
        int chisloIskomoe = Scaner.nextInt();
        int i;

        for ( i = 0 ;i < numberMassiv.length; i++){
                if (numberMassiv[i] == chisloIskomoe) {
                    System.out.println("искомое число есть в массиве под индексом "+ i  );
                    return;
                }

        }
                System.out.println("-1"); // по заданию при отсутствии числа в массиве, сделать вывод -1
                System.out.println("Искомое число "+chisloIskomoe+" не найденно в массиве");




    }

}
