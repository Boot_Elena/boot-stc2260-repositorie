public class Kalkuljator {

//    Сложение всех чисел, которые передаются в метод. Метод возвращает результат сложения
    public static int sumNumbers(int ... numbers){
        int result = 0;
        for (int i = 0; i < numbers.length; i++){
            result = result + numbers[i];
        }
        return result;
    }
//  Вычитание всех чисел, которые передаются в метод. Метод возвращает результат вычитания
    public static int minusNumbers(int ... numbers){
        int result = 0;
        for (int i = 0; i < numbers.length; i++){
            result = result - numbers[i];
        }
        return result;
    }

//    * Доп. задание: найти наибольшее число из чисел, которые передали в метод. И производить вычитание из него
    public static int minusNumbersBolhego(int ... numbers){

        int max = 0;

        for (int i = 0; i < numbers.length; i++){
            if (numbers[i] > max) {
                max = numbers[i];
            }
            else {
                max = max;
            }
        }
        int result = max;

        for (int i = 0; i < numbers.length; i++){
            max = max - numbers[i];
        }
                return max + result;
    }
//    Умножение всех чисел, которые передаются в метод. Метод возвращает результат умножения.
    public static int multiplicationNumbers(int ... numbers){
        int result = 1;
        for (int i = 0; i < numbers.length; i++){
            result = result * numbers[i];
        }
        return result;
    }

// Деление всех чисел, которые передаются в метод. При каждом следующем делении должна идти проверка:
//	* что делимое число должно быть больше делителя. Если условие не выполнено, то метод возвращает текущий результат деления.
//	* что делитель - положительное число. Если условие не выполнено, то метод возвращает текущий результат деления.
    public static double divisionNumbers(int ... numbers) {
        double result = 0;

        if (numbers[0] > 0) {      // дополнительно проверяется на положительное число с первого числа
            result = numbers[0];

            for (int i = 1; i < numbers.length; i++) {
                if (numbers[i] < 0) {
                    return result;
                }
                if (numbers[i] > 0) {
                    if (numbers[i] < result) {
                        result = result / numbers[i];
                    } else {
                        return result;
                    }
                }

            }

        }
        if (numbers[0] < 0) {

            return result;
        }
        return result;
    }

// Метод, высчитывающий факториал переданного числа. Должна быть проверка, что переданное число положительное.
    public static int faktorialNumbers(int faktorial) {
        if (faktorial > 0) {

            int result = 1;

            for (int i = 0; i < faktorial; i++) {
                result = result * (faktorial - i);
            }
            return result;
        }
        return -1;
    }

}
